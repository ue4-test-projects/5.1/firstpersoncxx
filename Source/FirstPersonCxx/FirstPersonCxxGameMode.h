#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FirstPersonCxxGameMode.generated.h"

UCLASS(minimalapi)
class AFirstPersonCxxGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	AFirstPersonCxxGameMode();
};
